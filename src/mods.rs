#[derive(Clone, Copy, PartialEq)]
pub struct Mods(pub u32);

impl Mods {
    /// Instance of [`Mods`] containing no mods.
    pub const MOD_NM: Self = Self(0);
    /// Instance of [`Mods`] containing No Fail.
    pub const MOD_NF: Self = Self(1 << 0);
    /// Instance of [`Mods`] containing Easy.
    pub const MOD_EZ: Self = Self(1 << 1);
    /// Instance of [`Mods`] containing Touch Device.
    pub const MOD_TD: Self = Self(1 << 2);
    /// Instance of [`Mods`] containing Hidden.
    pub const MOD_HD: Self = Self(1 << 3);
    /// Instance of [`Mods`] containing Hard Rock.
    pub const MOD_HR: Self = Self(1 << 4);

    /// Instance of [`Mods`] containing Double Time.
    pub const MOD_DT: Self = Self(1 << 6);

    /// Instance of [`Mods`] containing Half Time.
    pub const MOD_HT: Self = Self(1 << 8);
    /// Instance of [`Mods`] containing Nightcore.
    pub const MOD_NC: Self = Self(1 << 9);
    /// Instance of [`Mods`] containing Flashlight.
    pub const MOD_FL: Self = Self(1 << 10);

    /// Instance of [`Mods`] containing Spun Out.
    pub const MOD_SO: Self = Self(1 << 12);

    /// Parses an iterable that yields mod names like `"HD"`, `"dt"`, `"hr"`
    /// into a [`Mods`] instance.
    pub fn from_strs<T: AsRef<str>>(strs: impl IntoIterator<Item = T>) -> Self {
        let mut i = 0;
        for mstr in strs {
            let t = match mstr.as_ref().to_ascii_uppercase().as_ref() {
                "NF" => Self::MOD_NF.0,
                "EZ" => Self::MOD_EZ.0,
                "TD" => Self::MOD_TD.0,
                "HD" => Self::MOD_HD.0,
                "HR" => Self::MOD_HR.0,
                "DT" => Self::MOD_DT.0,
                "HT" => Self::MOD_HT.0,
                "NC" => Self::MOD_NC.0,
                "FL" => Self::MOD_FL.0,
                "SO" => Self::MOD_SO.0,
                _ => continue,
            };
            i |= t;
        }
        Self(i)
    }

    const MOD_SPEED_CHANGING: Mods = Self(Self::MOD_DT.0 | Self::MOD_HT.0 | Self::MOD_NC.0);
    const MOD_MAP_CHANGING: Mods =
        Self(Self::MOD_HR.0 | Self::MOD_EZ.0 | Self::MOD_SPEED_CHANGING.0);

    /// Returns `true` if at least one mod in `self` changes the map's speed.
    pub fn is_speed_changing(self) -> bool {
        self.0 & Self::MOD_SPEED_CHANGING.0 != 0
    }

    /// Returns `true` if at least one mod in `self` changes the map's layout.
    pub fn is_map_changing(self) -> bool {
        self.0 & Self::MOD_MAP_CHANGING.0 != 0
    }

    /// Returns the mods' resulting speed.
    pub fn speed(&self) -> f32 {
        let mut s = 1.0;
        // Double Time & Nightcore increases speed by 1.5
        if self.0 & (Self::MOD_DT.0 | Self::MOD_NC.0) != 0 {
            s = 1.5;
        }
        // Half Time reduces speed by 0.75
        if self.0 & Self::MOD_HT.0 != 0 {
            s *= 0.75;
        }
        s
    }

    /// Returns the mods' multiplier. Used in difficulty calculation in
    /// [`Difficulty`].
    ///
    /// [`Difficulty`]: super::diffcalc::Difficulty
    pub fn od_ar_hp_multiplier(&self) -> f32 {
        let mut m = 1.0;
        if self.has_hr() {
            m = 1.4;
        }
        if self.has_ez() {
            m *= 0.5;
        }
        m
    }

    /// Returns `true` if `self` contains No Fail.
    pub fn has_nf(&self) -> bool {
        self.0 & Self::MOD_NF.0 != 0
    }

    /// Returns `true` if `self` contains Easy.
    pub fn has_ez(&self) -> bool {
        self.0 & Self::MOD_EZ.0 != 0
    }

    /// Returns `true` if `self` contains Touch Device.
    pub fn has_td(&self) -> bool {
        self.0 & Self::MOD_TD.0 != 0
    }

    /// Returns `true` if `self` contains Hidden.
    pub fn has_hd(&self) -> bool {
        self.0 & Self::MOD_HD.0 != 0
    }

    /// Returns `true` if `self` contains Hard Rock.
    pub fn has_hr(&self) -> bool {
        self.0 & Self::MOD_HR.0 != 0
    }

    /// Returns `true` if `self` contains Double Time.
    pub fn has_dt(&self) -> bool {
        self.0 & Self::MOD_DT.0 != 0
    }

    /// Returns `true` if `self` contains Half Time.
    pub fn has_ht(&self) -> bool {
        self.0 & Self::MOD_HT.0 != 0
    }

    /// Returns `true` if `self` contains Hard Rock.
    pub fn has_nc(&self) -> bool {
        self.0 & Self::MOD_HR.0 != 0
    }

    /// Returns `true` if `self` contains Flashlight.
    pub fn has_fl(&self) -> bool {
        self.0 & Self::MOD_FL.0 != 0
    }

    /// Returns `true` if `self` contains Spun Out.
    pub fn has_so(&self) -> bool {
        self.0 & Self::MOD_SO.0 != 0
    }
}

use std::ops;

impl ops::BitAnd<Self> for Mods {
    type Output = Self;

    fn bitand(self, other: Self) -> Self {
        Self(self.0 & other.0)
    }
}

impl ops::BitAnd<u32> for Mods {
    type Output = Self;

    fn bitand(self, other: u32) -> Self {
        Self(self.0 & other)
    }
}

impl ops::BitOr<Self> for Mods {
    type Output = Self;

    fn bitor(self, other: Self) -> Self {
        Self(self.0 | other.0)
    }
}

impl ops::BitOr<u32> for Mods {
    type Output = Self;

    fn bitor(self, other: u32) -> Self {
        Self(self.0 | other)
    }
}
