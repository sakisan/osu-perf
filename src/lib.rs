//! An osu! performance point calculation library in pure, safe Rust.
//! This is a port of [oppai-ng](https://github.com/Francesco149/oppai-ng/).
//!
//! # Goals
//! 100% cross-platform code, straightforward & unencumbered API,
//! high performance & idiomatic implementation.
//!
//! The only dependency is the Rust standard library.
//!
//! # Current state
//! Usable, tested against top players' scores with a tolerance of +-2%.
//! Test suite available in `src/tests` and can be run with `cargo test`.
//! Benchmarks are available there too if you are on Rust nightly
//! (`cargo bench --feature nightly`).
//!
//! # Feature flags
//! `nightly`: recommended if you are on Rust nightly. If this is not enabled,
//! unstable features used in this crate are padded using stable features.

#![cfg_attr(feature = "nightly", feature(clamp, str_split_once, test))]

mod tests;

mod diffcalc;
mod hitobjects;
mod map;
mod mapstats;
mod mods;
mod pp;
mod timing;
mod vector2;

pub use diffcalc::Difficulty;
pub use hitobjects::{Hitobject, HitobjectKind};
pub use map::{Map, OsuMode, ParseMapError};
pub use mapstats::MapStatistics;
pub use mods::Mods;
pub use pp::Accuracy;
pub use pp::PpV2;
pub use timing::TimingPoint;
pub use vector2::Vector2;

/// Calculates performance points from the mods, combo and accuracy of a
/// score on the provided map. A `combo` of `None` implies a full combo.
///
/// # Panics
///
/// This will panic if the map is not in osu! game mode, as it is
/// the only supported game mode for now.
///
/// # Examples
///
/// ```no_run
/// use std::fs::File;
/// use std::io::BufReader;
/// use osu_perf::{Map, Accuracy, Mods, get_pp};
///
/// let file = File::open("/path/to/file.osu").unwrap();
/// let map = Map::parse(BufReader::new(file)).unwrap();
/// let accuracy = Accuracy { n300: 300, n100: 100, n50: 50, misses: 0 };
/// let pp = get_pp(&map, accuracy, None, Mods::MOD_NM);
/// println!("The play receives {:.2}pp", pp.pp.total);
/// ```
pub fn get_pp(beatmap: &Map, accuracy: Accuracy, combo: Option<u32>, mods: Mods) -> Performance {
    let diff = Difficulty::calc(beatmap, mods);
    let pp = PpV2::pp(
        beatmap,
        &diff.stats,
        diff.aim,
        diff.speed,
        combo,
        mods,
        accuracy,
        1,
        None,
    );
    Performance { pp, diff }
}

/// The results of a performance point calculation.
pub struct Performance {
    pub pp: PpV2,
    pub diff: Difficulty,
}
