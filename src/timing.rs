#[derive(Clone, Copy)]
pub struct TimingPoint {
    /// Start time, in milliseconds
    pub time: f32,
    pub ms_per_beat: f32,
    /// If false, ms_per_beat is `-100 * bpm_multiplier`
    pub change: bool,
}
