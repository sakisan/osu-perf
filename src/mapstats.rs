use super::mods::Mods;

/// The statistics of a map.
#[derive(Clone)]
pub struct MapStatistics {
    /// The map's approach rate.
    pub ar: f32,
    /// The map's overall difficulty.
    pub od: f32,
    /// The map's circle size.
    pub cs: f32,
    /// The map's health drain amount.
    pub hp: f32,
    /// The map's speed. Default value is 1.0, but will be changed when mods
    /// are applied, through the [`Mods`] struct and the [`with_mods`]
    /// function.
    ///
    /// [`Mods`]: super::mods::Mods
    /// [`with_mods`]: Self::with_mods
    pub speed: f32,
}

impl MapStatistics {
    const AR0_MS: f32 = 1800.0;
    const AR5_MS: f32 = 1200.0;
    const AR10_MS: f32 = 450.0;
    const AR_MS_STEP_1: f32 = (Self::AR0_MS - Self::AR5_MS) / 5.0;
    const AR_MS_STEP_2: f32 = (Self::AR5_MS - Self::AR10_MS) / 5.0;

    const OD_0MS: f32 = 80.0;
    const OD_10MS: f32 = 20.0;
    const OD_MS_STEP: f32 = (Self::OD_0MS - Self::OD_10MS) / 10.0;

    pub fn new(ar: f32, od: f32, cs: f32, hp: f32) -> Self {
        Self {
            ar,
            od,
            cs,
            hp,
            speed: 1.0,
        }
    }

    /// Applies the provided mods and returns a new instance.
    pub fn with_mods(&self, mods: Mods) -> Self {
        if !mods.is_map_changing() {
            return (*self).clone();
        }

        let speed = mods.speed();
        let mult = mods.od_ar_hp_multiplier();

        let mut ar = self.ar * mult;
        let mut ar_ms = if ar <= 5.0 {
            Self::AR0_MS - Self::AR_MS_STEP_1 * ar
        } else {
            Self::AR5_MS - Self::AR_MS_STEP_2 * (ar - 5.0)
        };
        // Stats must be capped to 0..10 before Hidden/Double Time,
        // which brings them to a range of -4.42..11.08 for OD and -5..11 for AR
        ar_ms = clamp(ar_ms, Self::AR10_MS, Self::AR0_MS); // ar_ms.max(Self::AR10_MS).min(Self::AR0_MS);
        ar_ms /= speed;
        ar = if ar_ms > Self::AR5_MS {
            (Self::AR0_MS - ar_ms) / Self::AR_MS_STEP_1
        } else {
            5.0 + (Self::AR5_MS - ar_ms) / Self::AR_MS_STEP_2
        };

        let mut od = self.od * mult;
        let mut od_ms = Self::OD_0MS - (Self::OD_MS_STEP * od).ceil();
        od_ms = od_ms.max(Self::OD_10MS).min(Self::OD_0MS);
        od_ms /= speed;
        od = (Self::OD_0MS - od_ms) / Self::OD_MS_STEP;

        let mut cs = self.cs;
        if mods.has_hr() {
            cs *= 1.3;
        }
        if mods.has_ez() {
            cs *= 0.5;
        }
        cs = cs.min(10.0);

        let hp = (self.hp * mult).min(10.0);

        Self {
            ar,
            od,
            cs,
            hp,
            speed,
        }
    }
}

#[cfg(feature = "nightly")]
#[inline]
fn clamp(val: f32, min: f32, max: f32) -> f32 {
    val.clamp(min, max)
}

#[cfg(not(feature = "nightly"))]
#[inline]
fn clamp(val: f32, min: f32, max: f32) -> f32 {
    debug_assert!(min <= max, "min must be less than or equal to max");
    if val < min {
        min
    } else if val > max {
        max
    } else {
        val
    }
}
