# osu-perf
**osu-perf** is an [osu!](https://osu.ppy.sh/) performance point calculation library in pure, safe Rust. No dependencies.
This is mostly a port of [oppai-ng](https://github.com/Francesco149/oppai-ng/).

## Installation
To your `Cargo.toml` file, add `osu_perf = { git = "https://gitlab.com/JackRedstonia/osu-perf" }`.

## Current state
Usable, tested against top players' scores.

Performance is as good as it can get, according to benchmarks from `cargo bench` and Linux [`perf`](https://perf.wiki.kernel.org/index.php/Main_Page).

This crate will not be published on [crates.io](https://crates.io) until there is a way to publish crates without a GitHub account and the API is stabilised.

What is here:
- `.osu` file parsing
- pp calculation and difficulty calculation
- testing, with +-2% tolerance

## License
This crate is licensed under the Lesser GNU GPL license version 3. See the `LICENSE` file for more information.